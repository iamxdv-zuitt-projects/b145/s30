/*==========================================================================*/
                         /* D I S C U S S I O N */
/*==========================================================================*/
// Create a server using Node.js
// Using Node.js
/* let http = require('http')

 http.createServer((request, response) => {

	if(request.url == "/" && request.method == "GET"){
 		response.writeHead(200, {'Content-Type' : 'text/plain'})
 		response.end('Data received')
 	};

 }).listen(4000) */


// Using Express.js
const express = require('express')

const app = express();
const port = 4000

// Middlewares

app.use(express.json())
app.use(express.urlencoded({extended: true}));

        // Mock database
        let users = [
            {
                email: "attackTitan@mail.com",
                username: "thisIsEren",
                password: "notABird",
                isAdmin: false
            },
            {
                email: "mrClean@mail.com",
                username: "AckermanLevi",
                password: "stillAlive",
                isAdmin: true
            },
            {
                email: "redScarf@mail.com",
                username: "Mikasa12",
                password: "whereIsEren",
                isAdmin: false
            },
        ];
        

        let loggedUser;

app.get('/', (req, res) => {
    res.send('Hello World')


});

/*
	Mini-Activity
	1. Make a route /hello as an endpoint
	2. Send a message saying hello from batch 145
	ENDS: 6:48PM
	Take a screenshot of your solution and send it in hangouts

*/

// Solution

    // Make a new route /hello endpoint
    app.get('/hello', (req, res) => {
        res.send('Hello from batch 145')
    
        });

// ****Using Post method at Express.js***
    app.post('/', (req, res) => {
        res.send('Hello I am from POST Method')
    
        });
    /*
	Mini Activity
	1. Change the message from the POST method
	2. res.send = Hello, I am <nameFromRequest>, my age is <ageFromRequest>. I could be described as <descriptionFromRequest>
	Send your POSTMAN output in Hangouts
	ENDS in 7:43PM
    */
// SOLUTION 

// res.send(`Hello, I am ${req.body.name} , my age is ${req.body.age}. I could be described as ${req.body.description} `)

                app.post('/helloMiniAct', (req, res) => 
                {
                res.send(`Hello, I am ${req.body.name}, my age is ${req.body.age}. I could be described as ${req.body.description}`)
            
                });

// ***REGISTER ROUTE***
app.post('/users', (req, res) => {

	console.log(req.body);

	let newUser = {

		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin

	};

	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered`)

});

// ***LOG IN ROUTE***

app.post('/users/login', (req, res) => {

	console.log(req.body);

	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password
	})

	if(foundUser !== undefined){
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});
		
		foundUser.index = foundUserIndex

		loggedUser = foundUser

		res.send(`Thank you for logging in.`)

	} else {

		loggedUser = foundUser
		res.send('Sorry, wrong credentials')
	}
});

//*** CHANGE PASSWORD ROUTE***

app.put('/users/change-password', (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been changed`

			break;

		} else {

			message = `User does not exist`
		}
	}

	res.send(message)
})



/*  A C T I V I T Y 

1. Create a GET route that will access the /home route that will print out
 a simple message.

    
2. Process a GET request at the /home route using postman

3. Create a GET route that will access the /users route that will retrieve all the users in the mock database.

4. Process a GET request at the /users route using postman
5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database.

6. Process a DELETE request at the /delete-user route using the Postman
7. Export the POSTMAN collection and save it inside the root folder of our application

8. Create a git repository named S31
9. Initialize a local git repository, add the remote link and push to git with the commit message of "Add Activity Code"

Add the link in Boodle 

*/


// item 1
app.get('/home', (req, res) => {
    res.send('Hi, Its Great to see you!')
});

// item 3
app.get('/users', (req, res) => {
    let users2 = [
        {
            
            username: "thisIsEren",
            password: "notABird",
            
        },
        {
            
            username: "AckermanLevi",
            password: "stillAlive",
            
        },
        {
            
            username: "Mikasa12",
            password: "whereIsEren",
           
        },
    ];
	res.send(users2)

});

// item 5

app.delete('/delete-user', (req, res) => {
    res.send(`User ${req.body.username} has been deleted`)

    });

app.listen(port, () => console.log(`server is running at port ${port}`));

/*==========================================================================*/
                    /* END OF DISCUSSION and ACTIVITY */
/*==========================================================================*/





